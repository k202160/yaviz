symlink_to_builddir("../pvExample.py" "pvExample.py")
symlink_to_builddir("../coupling.xml" "coupling.xml")
symlink_to_builddir("../coupling.xsd" "coupling.xsd")
configure_file("yaviz.yaml.in" "yaviz.yaml")

add_test(NAME ex2_reg3dgrid COMMAND mpirun -n 1 ../example_simulation : -n 1 ../../yaviz)

