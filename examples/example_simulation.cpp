#include <iostream>
#include <mpi.h>
#include <vector>
#include <array>
#include <thread>
#include <chrono>
#include <cmath>

extern "C"{
#include <yac_interface.h>
}

std::vector<double> linspace(double x0, double x1, int len){
  std::vector<double> result(len);
  double spacing = (x1-x0)/(len-1);
  for(int i=0; i<len; ++i){
    result[i] = x0+i*spacing;
  }
  return result;
}

double test_ana_fcos(double lon, double lat) {

  double dp_length = 1.2 * M_PI;
  double coef = 2.0;
  double coefmult = 1.0;
  return coefmult * (coef - cos(M_PI * acos(cos(lon)*cos(lat) )/dp_length));
}

double test_gulfstream(double t, double lon, double lat) {
  // Analytical Gulf Stream
  double gf_coef = 1.0; // Coefficient for Gulf Stream term (0.0 = no Gulf Stream)
  double YAC_RAD = acos(-1)/180;
  double gf_ori_lon = -80.0 * YAC_RAD; // Origin of the Gulf Stream
  double gf_ori_lat = 25.0 * YAC_RAD; // Origin of the Gulf Stream
  double gf_end_lon = -1.8 * YAC_RAD; // End point of the Gulf Stream
  double gf_end_lat = 50.0 * YAC_RAD; // End point of the Gulf Stream
  double gf_dmp_lon = -25.5 * YAC_RAD; // Point of the Gulf Stream decrease
  double gf_dmp_lat = 55.5 * YAC_RAD; // Point of the Gulf Stream decrease

  gf_dmp_lon += M_PI*sin(t);
  gf_ori_lat += 0.05*M_PI*sin(t/2);

  double dr0 = sqrt(pow(gf_end_lon - gf_ori_lon, 2.0) +
                    pow(gf_end_lat - gf_ori_lat, 2.0));
  double dr1 = sqrt(pow(gf_dmp_lon - gf_ori_lon, 2.0) +
                    pow(gf_dmp_lat - gf_ori_lat, 2.0));

  if (lon > M_PI) lon -= 2.0 * M_PI;
  if (lon < -M_PI) lon += 2.0 * M_PI;

  double dx = lon - gf_ori_lon;
  double dy = lat - gf_ori_lat;
  double dr = sqrt(dx * dx + dy * dy);
  double dth = atan2(dy, dx);
  double dc = 1.3 * gf_coef;
  if (dr > dr0) dc = 0.0;
  if (dr > dr1) dc = dc * cos(M_PI*0.5*(dr-dr1)/(dr0-dr1));
  return test_ana_fcos(lon, lat) +
    (std::max(1000.0 * sin(0.4 * (0.5*dr+dth) +
			   0.007*cos(50.0*dth) + 0.37*M_PI),999.0) - 999.0) * dc;
}

void update_field(int t, std::vector<double>& field, int offset){
  double pi = acos(-1);
  for(size_t i = 0; i<field.size(); ++i){
    double x = (2*pi*(i%offset))/offset - pi;
    double y = -pi/2+(pi*(i/offset))/offset;
    field[i] = test_gulfstream(t,x,y);
  }
}

int main(int argc, char** argv){
  MPI_Init(&argc, &argv);

  int delay = 200;
  if(argc > 1)
    delay = std::atoi(argv[1]);

  yac_cinit("coupling.xml", "coupling.xsd");
  int comp_id;
  yac_cdef_comp("example_simulation", &comp_id);

  // grid initialization
  int cyclic[2] = {0, 0};
  double pi = acos(-1.);
  std::vector<double> x_vertices = linspace(-pi, pi, 100);
  std::vector<double> y_vertices = linspace(-pi/2, pi/2, 100);
  int nbr_vertices[2] = {(int)x_vertices.size(), (int)y_vertices.size()};
  int grid_id;
  yac_cdef_grid_reg2d("example_grid2", &nbr_vertices[0], &cyclic[0],
		      x_vertices.data(), y_vertices.data(), &grid_id);
  int points_id;
  yac_cdef_points_reg2d(grid_id, &nbr_vertices[0], YAC_LOCATION_CORNER,
			x_vertices.data(), y_vertices.data(), &points_id);
  int points_size = yac_get_points_size(points_id);
  std::vector<double> field(points_size);
  std::fill(field.begin(), field.end(), -100);

  // field defintiion
  int field_id;
  yac_cdef_field("example_field", comp_id, &points_id, 1, &field_id);

  std::array<std::vector<double>, 3> vector_field;
  vector_field[0].resize(points_size, 0);
  vector_field[1].resize(points_size, 1);
  vector_field[2].resize(points_size, 2);
  int vector_field_id;
  yac_cdef_field("example_vector_field", comp_id, &points_id, 1, &vector_field_id);
  
  int ierror = -1;
  yac_csearch(&ierror);

  double* field_ptr = field.data();
  double** field_ptr_ptr = &field_ptr;

  std::vector<double*> vector_field_ptr = {vector_field[0].data(), vector_field[1].data(), vector_field[2].data()};
  std::vector<double**> vector_field_ptr_ptr = {&vector_field_ptr[0], &vector_field_ptr[1], &vector_field_ptr[2]};

  int info = 0;
  bool all_fields_done = false;
  int t = 0;
  while(!all_fields_done){
    update_field(t, field, x_vertices.size());
    update_field(t, vector_field[0], x_vertices.size());
    all_fields_done = true;
    yac_cput(field_id, 1, &field_ptr_ptr, &info, &ierror);
    all_fields_done &= (info>=2 || ierror != 0);
    yac_cput(vector_field_id, 3, vector_field_ptr_ptr.data(), &info, &ierror);
    all_fields_done &= (info>=2 || ierror != 0);
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(std::chrono::milliseconds(delay));
    std::cout << "simulation: " << t << std::endl;
    t++;
  }

  yac_cfinalize();
  MPI_Finalize();
  return 0;
}
