
from paraview import catalyst

options = catalyst.Options()

counter = 0

def catalyst_execute(info):
    global counter
    counter += 1

def catalyst_finalize():
    global counter
    expected = int(catalyst.get_args()[0][1:])
    print(f"Checking number of timsteps: counted: {counter}, expected: {expected}")
    if counter != expected:
        exit(666)

    print("Test passed!")

