#include <cmath>
#include <vector>
#include <tuple>
#include <map>
#include <algorithm>

#include <catalyst_conduit.hpp>

#include "outstream.h"
#include "mpihelper.h"

namespace CDI {
extern "C" {
#include <cdi.h>
}
}

extern "C" {
#include <yac_interface.h>
}

namespace YAVIZ {

  // utility function for chekcing the unit of the axis
  inline void ensureRad(int gridID, int cdi_var_key, std::vector<double>& values){
    int len = 7;
    std::string unit(len, 0);
    CDI::cdiInqKeyString(gridID, cdi_var_key, CDI_KEY_UNITS, unit.data(), &len);
    if (unit.substr(0, 6) == "degree"){
      std::transform(values.begin(), values.end(), values.begin(),
        [](double x){ return M_PI*x/180.;});
    }else{
      if(unit.substr(0,3) != "rad"){
	throw std::runtime_error("invalid unit coordinates");
      }
    }
  }

  inline std::tuple<double, double, double> latlon2sphere(double lon, double lat, double z){
    using std::sin, std::cos;
    return std::tuple((1+z)*sin(-lon)*cos(lat), (1+z)*cos(lon)*cos(lat), (1+z)*sin(lat));
  }

  void cdi_grid_factory(conduit_cpp::Node& grid_node){
    std::string filename = grid_node["cdi_filename"].as_string();
    std::string projection = grid_node.has_child("projection")?grid_node["projection"].as_string():"spherical";
    bool tetrahedralize = grid_node["tetrahedralize"].to_int();
    infoStr << "Reading file " << filename << std::endl;
    int streamID = CDI::streamOpenRead(filename.c_str());
    infoStr << "Got stream ID " << streamID << " from CDI" << std::endl;
    if(streamID < 0)
      throw std::runtime_error("failed to open file " + filename + "(err no: " + std::to_string(streamID) + ")");
    infoStr << "Read vList" << std::endl;
    int vlistID = CDI::streamInqVlist(streamID);
    infoStr << "Got vListID " << vlistID << std::endl;
    if(vlistID < 0)
      throw std::runtime_error("Invalid VList");

    std::string varname = grid_node["varname"].as_string();
    infoStr << "looking for variable " << varname << std::endl;
    int nvars = CDI::vlistNvars(vlistID);
    infoStr << "Found " << nvars << " variables" << std::endl;
    char name[CDI_MAX_NAME];
    int varID = 0;
    for(; varID < nvars; ++varID){
      CDI::vlistInqVarName(vlistID, varID, name);
      if(std::string(name) == varname){
        infoStr << "Found variable " << varname << " with variableID" << varID << std::endl;
        break;
      }
    }
    if(varID == nvars)
      throw std::runtime_error("Cant find variable " +varname+" in file " + filename);

    int gridID, zaxisID, taxisID;
    CDI::vlistInqVar(vlistID, varID, &gridID, &zaxisID, &taxisID);

    int size = CDI::gridInqSize(gridID);
    size_t local_size = size/MPIHelper::size();
    int local_offset = MPIHelper::rank()*local_size;
    if(MPIHelper::size()-1 == MPIHelper::rank())
      local_size = size - (MPIHelper::size()-1)*local_size; // the last rank gets the remainder

    std::vector<double> xvals(local_size);
    std::vector<double> yvals(local_size);
    CDI::gridInqXvalsPart(gridID, local_offset, local_size, xvals.data());
    CDI::gridInqYvalsPart(gridID, local_offset, local_size, yvals.data());
    ensureRad(gridID, CDI_XAXIS, xvals);
    ensureRad(gridID, CDI_YAXIS, yvals);

    // read and compute cells
    size_t ncorner = CDI::gridInqNvertex(gridID);
    if(tetrahedralize && ncorner!=3)
      throw std::runtime_error("Tetrahedralization only works with triangular meshes.");
    int local_nbr_bounds = ncorner*local_size;
    std::vector<double> xbounds(local_nbr_bounds);
    std::vector<double> ybounds(local_nbr_bounds);
    CDI::gridInqXboundsPart(gridID, ncorner*local_offset, local_nbr_bounds, xbounds.data());
    CDI::gridInqYboundsPart(gridID, ncorner*local_offset, local_nbr_bounds, ybounds.data());
    ensureRad(gridID, CDI_XAXIS, xbounds);
    ensureRad(gridID, CDI_YAXIS, ybounds);

    infoStr << "Closing CDI stream(" << streamID << ")" << std::endl;
    CDI::streamClose(streamID);

    // remove duplicates and build connectivities
    struct IndexedPoint{
      double x, y;
      size_t idx;
    };
    std::vector<IndexedPoint> ipoints(ncorner*local_size);
    for(size_t i = 0; i<xbounds.size(); ++i)
      ipoints[i] = {.x = xbounds[i],
                    .y = ybounds[i],
                    .idx = i};

    auto compare = [](const IndexedPoint& a, const IndexedPoint& b){
      constexpr double threshold = 1e-22;
      if(std::fabs(a.x-b.x) > threshold)
        return a.x < b.x;
      if(std::fabs(a.y-b.y) > threshold)
        return a.y < b.y;
      return false;
    };
    std::sort(ipoints.begin(), ipoints.end(),
      compare);

    // copy unique points to vectors and build connectivities
    std::vector<int> cell_corners(ncorner*local_size);
    std::vector<int> ncorners(local_size);
    std::fill(ncorners.begin(), ncorners.end(), ncorner);

    xbounds[0] = ipoints[0].x;
    ybounds[0] = ipoints[0].y;
    cell_corners[ipoints[0].idx] = 0;
    size_t nbr_points = 1;
    for(size_t i = 1; i < ipoints.size(); ++i){
      if(compare(ipoints[i-1], ipoints[i]) || compare(ipoints[i], ipoints[i-1])){
        xbounds[nbr_points] = ipoints[i].x;
        ybounds[nbr_points] = ipoints[i].y;
        nbr_points++;
      }
      cell_corners[ipoints[i].idx] = nbr_points-1;
    }
    xbounds.resize(nbr_points);
    ybounds.resize(nbr_points);

    // filter out double corners in cells
    std::vector<int> offset(local_size);
    if (ncorner > 3){
      int off = 0;
      for(size_t cell=0; cell<local_size; ++cell){
        ncorners[cell] = 1;
        offset[cell] = off;
        cell_corners[off] = cell_corners[cell*ncorner];
        off++;
        for(size_t corner=1; corner < ncorner; ++corner){
          if(cell_corners[off-1] != cell_corners[cell*ncorner+corner]){
            cell_corners[off] = cell_corners[cell*ncorner+corner];
            ncorners[cell]++;
            off++;
          }
        }
      }
      cell_corners.resize(off);
    }else{ // ncorner == 3
      for(size_t i = 0; i<local_size; ++i){
        offset[i] = ncorner*i;
      }
    }

    // If we're using latlon projection we add the points add the left/right boundary to the point set
    // These additional points are later moved to the other side
    if(projection == "latlon"){
      for(size_t cell = 0; cell < ncorners.size(); ++cell){
        for(int corner = 0; corner < ncorners[cell]; ++corner){
          int corner_idx = cell_corners[offset[cell]+corner];
          if(xvals[cell] < -M_PI/2 && xbounds[corner_idx] > M_PI/2){
            xbounds.push_back(xbounds[corner_idx] - 2*M_PI);
            ybounds.push_back(ybounds[corner_idx]);
            cell_corners[offset[cell]+corner] = xbounds.size()-1;
          }
          if(xvals[cell] > M_PI/2 && xbounds[corner_idx] < -M_PI/2){
            xbounds.push_back(xbounds[corner_idx] + 2*M_PI);
            ybounds.push_back(ybounds[corner_idx]);
            cell_corners[offset[cell]+corner] = xbounds.size()-1;
          }
        }
      }
      nbr_points = xbounds.size();
    }


    // register to yac
    int yac_grid_id;
    yac_cdef_grid_unstruct(grid_node.name().c_str(), xbounds.size(), local_size,
      ncorners.data(), xbounds.data(), ybounds.data(),
      cell_corners.data(), &yac_grid_id);
    grid_node["yac_grid_id"] = yac_grid_id;

    int cell_points_id;
    yac_cdef_points_unstruct(yac_grid_id, xvals.size(), YAC_LOCATION_CELL,
      xvals.data(), yvals.data(), &cell_points_id);
    grid_node["yac_cell_points_id"] = cell_points_id;

    int corner_points_id;
    yac_cdef_points_unstruct(yac_grid_id, xbounds.size(), YAC_LOCATION_CORNER,
      xbounds.data(), ybounds.data(), &corner_points_id);
    grid_node["yac_corner_points_id"] = corner_points_id;

    // move the additional points to the other side
    if(projection == "latlon"){
      for(size_t i = nbr_points; i<xbounds.size(); ++i){
        if(xbounds[i] > 0)
          xbounds[i] -= 2*M_PI;
        else
          xbounds[i] += 2*M_PI;
      }
    }

    grid_node["data/topologies/mesh/type"] = "unstructured";
    grid_node["data/topologies/mesh/coordset"] = "coords";

    // introduce z axis
    std::vector<double> zbounds;
    double oz = grid_node["zaxis/oz"].to_double();
    zbounds.resize(nbr_points);
    std::fill(zbounds.begin(), zbounds.end(), oz);
    size_t levels = grid_node["zaxis/levels"].to_int();
    levels = levels==0?1:levels;
    if(levels == 1){
      grid_node["data/topologies/mesh/elements/shape"] = "polygonal";
      grid_node["data/topologies/mesh/elements/sizes"] = std::move(ncorners);
      grid_node["data/topologies/mesh/elements/connectivity"] = std::move(cell_corners);
      grid_node["data/topologies/mesh/elements/offsets"] = std::move(offset);
    }else{
      double dz = grid_node["zaxis/dz"].to_double();
      dz = dz==0?1:dz;
      std::vector<double> zaxis(levels);
      std::generate(zaxis.begin(), zaxis.end(), [&,n = 0] () mutable { return oz + dz*(n++); });

      // copy horizontal grid `levels` times
      xbounds.reserve(xbounds.size()*levels);
      ybounds.reserve(xbounds.size()*levels);
      zbounds.reserve(xbounds.size()*levels);
      ncorners.reserve(ncorners.size()*levels);
      offset.reserve(offset.size()*levels);
      cell_corners.reserve(cell_corners.size()*levels);
      size_t cc_size = cell_corners.size();
      for(size_t i=1; i<levels; ++i){
        xbounds.insert(xbounds.end(), xbounds.begin(), xbounds.begin()+nbr_points);
        ybounds.insert(ybounds.end(), ybounds.begin(), ybounds.begin()+nbr_points);
        zbounds.insert(zbounds.end(), nbr_points, zaxis[i]);

        // horizontal connectivities
        ncorners.insert(ncorners.end(), ncorners.begin(), ncorners.begin()+local_size);
        std::transform(offset.begin(), offset.begin()+local_size, std::back_inserter(offset),
          [&](auto o){return o+i*cc_size;});
        std::transform(cell_corners.begin(), cell_corners.begin()+cc_size, std::back_inserter(cell_corners),
          [&](int cc){ return cc+i*nbr_points;});
      }

      if(!tetrahedralize){
        // vertical connectivities
        std::map<std::tuple<int,int>, int> edge_idx; // stores the index of the first vertical element in the column
        std::vector<int> polyhedral_connectivity;
        std::vector<int> polyhedral_sizes;
        std::vector<int> polyhedral_offsets;
        for(size_t i=0; i<local_size; ++i){ // for each horizontal element
          std::vector<int> element_edges;
          for(int j=0; j<ncorners[i]; ++j){ // for each edge
            int v1 = cell_corners[offset[i]+j];
            int v2 = cell_corners[offset[i]+((j+1)%ncorners[i])];
            std::tuple<int,int> edge = v1<v2?std::tuple<int,int>{v1,v2}:std::tuple<int,int>{v2,v1};
            if(!edge_idx.count(edge) != 0){
              edge_idx[edge] = offset.size();
              for(size_t k=1; k<levels; ++k){
                ncorners.push_back(4); // each vertical element has 4 corners
                offset.push_back(cell_corners.size());
                cell_corners.insert(cell_corners.end(), {int(v1+(k-1)*nbr_points),
                                                         int(v2+(k-1)*nbr_points),
                                                         int(v2+k*nbr_points),
                                                         int(v1+k*nbr_points)});
              }
            }
            element_edges.push_back(edge_idx[edge]);
          }
          // add polyhedral elements of first layer
          polyhedral_offsets.push_back(polyhedral_connectivity.size());
          polyhedral_sizes.push_back(ncorners[i]+2);
          polyhedral_connectivity.push_back(i); // lower horizontal element
          polyhedral_connectivity.push_back(i+local_size); // upper horizontal element
          std::copy(element_edges.begin(), element_edges.end(),
            std::back_inserter(polyhedral_connectivity));
        }
        // "copy" the first layer to the upper layers
        for(size_t k = 2; k<levels; ++k){
          for(size_t i=0; i<local_size; ++i){ // for each horizontal element
            polyhedral_offsets.push_back(polyhedral_connectivity.size());
            polyhedral_sizes.push_back(ncorners[i]+2);
            polyhedral_connectivity.push_back(i+(k-1)*local_size); // lower horizontal element
            polyhedral_connectivity.push_back(i+k*local_size); // upper horizontal element
            for(int j=0; j<ncorners[i];++j){
              polyhedral_connectivity.push_back(polyhedral_connectivity[polyhedral_offsets[i]+j+2]+k-1);
            }
          }
        }
        grid_node["data/topologies/mesh/elements/shape"] = "polyhedral";
        grid_node["data/topologies/mesh/elements/sizes"] = std::move(polyhedral_sizes);
        grid_node["data/topologies/mesh/elements/connectivity"] = std::move(polyhedral_connectivity);
        grid_node["data/topologies/mesh/elements/offsets"] = std::move(polyhedral_offsets);
        grid_node["data/topologies/mesh/subelements/shape"] = "polygonal";
        grid_node["data/topologies/mesh/subelements/sizes"] = std::move(ncorners);
        grid_node["data/topologies/mesh/subelements/connectivity"] = std::move(cell_corners);
        grid_node["data/topologies/mesh/subelements/offsets"] = std::move(offset);

      }else{ // tetrahedralize
        std::vector<int> tetrahedral_connectivity;
        tetrahedral_connectivity.reserve(4*3*levels*local_size);
        // add 3 tetrahedrons for each element in each layer
        for(size_t k = 1; k<levels; ++k){
          for(size_t i=0; i<local_size; ++i){ // for each horizontal element
            int v1 = cell_corners[3*i] + (k-1)*nbr_points;
            int v2 = cell_corners[3*i+1] + (k-1)*nbr_points;
            int v3 = cell_corners[3*i+2] + (k-1)*nbr_points;
            // tet 1
            tetrahedral_connectivity.push_back(v1);
            tetrahedral_connectivity.push_back(v2);
            tetrahedral_connectivity.push_back(v3);
            tetrahedral_connectivity.push_back(v1+nbr_points);

            // tet 2
            tetrahedral_connectivity.push_back(v2);
            tetrahedral_connectivity.push_back(v1+nbr_points);
            tetrahedral_connectivity.push_back(v2+nbr_points);
            tetrahedral_connectivity.push_back(v3+nbr_points);

            // tet 3
            tetrahedral_connectivity.push_back(v2);
            tetrahedral_connectivity.push_back(v3);
            tetrahedral_connectivity.push_back(v1+nbr_points);
            tetrahedral_connectivity.push_back(v3+nbr_points);
          }
        }
        grid_node["data/topologies/mesh/elements/shape"] = "tet";
        grid_node["data/topologies/mesh/elements/connectivity"] = std::move(tetrahedral_connectivity);
      }

    }

    if(projection=="spherical"){
      // use spherical coordinates
      for(size_t i=0;i<xbounds.size();++i){
        std::tie(xbounds[i], ybounds[i], zbounds[i]) = latlon2sphere(xbounds[i], ybounds[i], zbounds[i]);
      }
    }

    grid_node["data/coordsets/coords/type"] = "explicit";
    grid_node["data/coordsets/coords/values/x"] = std::move(xbounds);
    grid_node["data/coordsets/coords/values/y"] = std::move(ybounds);
    grid_node["data/coordsets/coords/values/z"] = std::move(zbounds);

    // add field "layer"
    grid_node["data/fields/vert_layer/association"] = "vertex";
    grid_node["data/fields/vert_layer/topology"] = "mesh";
    grid_node["data/fields/vert_layer/volume_dependent"] = false;
    std::vector<int> layer(levels*nbr_points);
    for(size_t i=0;i<levels*nbr_points;++i)
      layer[i] = i/nbr_points;
    grid_node["data/fields/vert_layer/values"] = layer;

    grid_node.print();
  }
}
