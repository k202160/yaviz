#include <iostream>
#include <fstream>
#include <regex>

#include <catalyst.hpp>
#include <catalyst_conduit.hpp>
#include <mpi.h>

extern "C"{
#include <yac_interface.h>
}

#include "mpihelper.h"

#include "outstream.h"
#include "timer.h"

namespace YAVIZ {
  void initialization(conduit_cpp::Node& config);
  void prepare(conduit_cpp::Node& config);
  void timeloop(conduit_cpp::Node& config);
  void finalize(conduit_cpp::Node& config);
}

using namespace YAVIZ;

int main(int argc, char** argv){

  MPIHelper::initialize(&argc, &argv);
  std::string config_name;
  if(argc < 2)
    config_name = "yaviz.yaml";
  else
    config_name = std::string(argv[1]);
  std::ifstream config_f(config_name);
  std::string config_str(std::istreambuf_iterator<char>{config_f}, {});
  conduit_cpp::Node config;
  conduit_node_parse(c_node(&config), config_str.c_str(), "yaml");

  initialization(config);

  prepare(config);

  infoStr << "yac_csearch()..." << std::endl;
  int ierror = -1;
  Timer search_timer;
  yac_csearch(&ierror);
  search_timer.toc();
  timingStr << "yac search: " << search_timer.seconds() << "s" << std::endl;

  timeloop(config);

  finalize(config);
  return 0;
}
