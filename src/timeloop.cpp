#include <catalyst_conduit.hpp>
#include <catalyst.hpp>

#include "mpihelper.h"

extern "C" {
#include <yac_interface.h>
#include <mtime_calendar.h>
#include <mtime_datetime.h>
#include <mtime_timedelta.h>
#include <mtime_eventHandling.h>
}

#include "timer.h"
#include "outstream.h"

namespace YAVIZ {

  // use mtime to build the time loop based on events
  void timeloop(conduit_cpp::Node& config){

    initCalendar(PROLEPTIC_GREGORIAN); // TODO: can we request the calender from yac?

    // create an event for each field
    _eventGroup* event_group = newEventGroup("YAVIZ");
    if(!event_group)
      throw std::runtime_error("Cannot create event group using mtime");

    char* start = yac_cget_start_datetime();
    char* end = yac_cget_end_datetime();

    conduit_cpp::Node execute_node = config["catalyst_execute"];
    int nbr_channels = execute_node["catalyst/channels"].number_of_children();
    for(int c = 0; c < nbr_channels; ++c){
      conduit_cpp::Node grid_node = execute_node["catalyst/channels"].child(c);
      int nbr_fields = grid_node["data/fields"].number_of_children();
      for(int f = 0; f < nbr_fields; ++f){
        conduit_cpp::Node field_node = grid_node["data/fields"].child(f);
        if(!field_node.has_child("yac_field_id"))
          continue; // continue if this is not a "yac field"
        char modeltimestep[MAX_DATETIME_STR_LEN];
        yac_cget_model_timestep_id(field_node["yac_field_id"].to_int(), modeltimestep);
        std::string event_name = grid_node.name()+"/data/fields/"+field_node.name();
        _event* event = newEvent(event_name.c_str(),NULL,
          start,end, modeltimestep, NULL);
        if(!event || !addNewEventToEventGroup(event, event_group))
          throw std::runtime_error("Cannot register event for field " + field_node.name());
      }
    }

    // if a timestep is specified register yaviz also as an event
    _event* yaviz_event = NULL;
    if(config.has_child("timestep")){
      std::string yaviz_timestep = config["timestep"].as_string();
      yaviz_event = newEvent("YAVIZ",NULL,
        start,end, yaviz_timestep.c_str(), NULL);
      if(!yaviz_event)
        throw std::runtime_error("Cannot create event for YAVIZ ");
    }

    _datetime* current_date = newDateTime(start);
    _datetime* next_date = newDateTime(start);
    _datetime* date_buf = newDateTime(start);
    _datetime* start_date = newDateTime(start);
    _datetime* end_date = newDateTime(end);
    _timedelta* td = newTimeDelta("PT0S");
    char date_str_buf[MAX_DATETIME_STR_LEN];
    execute_node["catalyst/state/cycle"] = 0;
    Timer yac_timer;
    Timer catalyst_timer;
    while  (compareDatetime(current_date,end_date) < 0){
      replaceDatetime(next_date, current_date);
      replaceDatetime(end_date, next_date);
      infoStr << "YAVIZ Time: " << datetimeToString(current_date, date_str_buf) << std::endl;
      execute_node["catalyst/state/parameters/time"] = date_str_buf;

      for (_event* e = event_group->rootEvent; e != NULL; e = e->nextEventInGroup){
        if (isCurrentEventActive(e,current_date, NULL, NULL)){
          char event_name_buf[256];
          eventToString(e,event_name_buf);
          infoStr << event_name_buf << " is active" << std::endl;
          conduit_cpp::Node field_node = execute_node["catalyst/channels"][event_name_buf];
          int ierror, info;
          yac_timer.tic();
          yac_cget_(field_node["yac_field_id"].to_int(), field_node["yac_collection_size"].to_int(),
            field_node["values"].as_double_ptr(), &info, &ierror);
          yac_timer.toc();
          if(ierror != 0)
            warnStr << "yac_cget_ returned error code " << ierror << " for field " << event_name_buf << std::endl;
          infoStr << "yac_cget_ finished with info = " << info << std::endl;
        }
        if(compareDatetime(next_date, getTriggerNextEventAtDateTime(e, current_date, date_buf)) > 0
          && compareDatetime(date_buf, current_date) > 0)
          replaceDatetime(date_buf, next_date);
      }
      // if yaviz event is NULL execute in every iteration!
      if (!yaviz_event || isCurrentEventActive(yaviz_event,current_date, NULL, NULL)){
        infoStr << "calling catalyst_execute" << std::endl;
        execute_node["catalyst/state/cycle"] = execute_node["catalyst/state/cycle"].to_int()+1;
        execute_node["catalyst/state/timestep"] = execute_node["catalyst/state/cycle"].to_int();
        execute_node["catalyst/state/time"]
          = getTotalSecondsTimeDelta(getTimeDeltaFromDateTime(current_date, start_date, td), start_date);
        catalyst_timer.tic();
        catalyst_status result = catalyst_execute(c_node(&execute_node));
        if(result != catalyst_status_ok)
          throw std::runtime_error("catalyst_execute failed with status code " + std::to_string(result));
        catalyst_timer.toc();
      }
      if(compareDatetime(next_date, getTriggerNextEventAtDateTime(yaviz_event, current_date, date_buf)) > 0
        && compareDatetime(date_buf, current_date) > 0)
        replaceDatetime(date_buf, next_date);
    }

    timingStr << "yac timer: " << yac_timer.seconds() << "s" << std::endl;
    timingStr << "catalyst_execute timer: " << catalyst_timer.seconds() << "s" << std::endl;

    deallocateEventGroup(event_group);
    deallocateDateTime(current_date);
    deallocateDateTime(next_date);
    deallocateDateTime(date_buf);
    deallocateDateTime(start_date);
    deallocateDateTime(end_date);
    deallocateTimeDelta(td);
    freeCalendar();

  }
}
