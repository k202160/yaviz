
from paraview import catalyst
from paraview.simple import *

options = catalyst.Options()
options.EnableCatalystLive = 1

channel = catalyst.get_args()[0]
print(f"Registering producer for channel {channel}")
producer = TrivialProducer(registrationName=channel)

def catalyst_finalize():
    producer.UpdatePipeline()
    expected_points = int(catalyst.get_args()[1][1:])
    expected_cells = int(catalyst.get_args()[2][1:])
    points = producer.GetDataInformation().DataInformation.GetNumberOfPoints()
    print(f"Checking number of points: counted: {points}, expected: {expected_points}")
    if points != expected_points:
        exit(666)
    cells = producer.GetDataInformation().DataInformation.GetNumberOfCells()
    print(f"Checking number of points: counted: {cells}, expected: {expected_cells}")
    if cells != expected_cells:
        exit(666)

    print("Test passed!")
