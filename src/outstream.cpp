#include "outstream.h"

namespace YAVIZ {
  OutStream& operator<< (OutStream &s, std::ostream& (*f)(std::ostream &)) {
    if(s.active)
      f(s._outStream);
    return s;
  }

  OutStream& operator<< (OutStream &s, std::ostream& (*f)(std::ios &)) {
    if(s.active)
      f(s._outStream);
    return s;
  }

  OutStream& operator<< (OutStream &s, std::ostream& (*f)(std::ios_base &)) {
    if(s.active)
      f(s._outStream);
    return s;
  }

  OutStream warnStr = OutStream(std::cout, 35 /*=yellow*/);
  OutStream infoStr = OutStream(std::cout);
  OutStream timingStr = OutStream(std::cout, 34 /*=blue*/);

  void OutStream::setVerbosity(int v){
    int rank = MPIHelper::rank();
    if(v >= 1)
      warnStr.active = rank==0;
    if(v >= 2)
      infoStr.active = rank==0;
    if(v >= 3)
      infoStr.active = true;
  }

  void OutStream::activateProfiling(bool active){
    timingStr.active = active;
  }
}
