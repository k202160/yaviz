#ifndef TIMER_H
#define TIMER_H

#include <chrono>

namespace YAVIZ {

  class Timer{
    using clock = std::chrono::high_resolution_clock;
    using duration_type = std::chrono::duration<float>;
    duration_type _duration = duration_type(0.0);
    std::chrono::time_point<clock> _start = clock::now();

  public:
    void tic(){
      _start = clock::now();
    }

    void toc(){
      _duration += clock::now()-_start;
    }

    void reset(){
      _duration = duration_type(0.0);
    }

    float seconds() const{
      return _duration.count();
    }
  };
}

#endif
