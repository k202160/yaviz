#include <catalyst.hpp>
#include <catalyst_conduit.hpp>

#include "mpihelper.h"

extern "C"{
#include <yac_interface.h>
}

#include "outstream.h"
#include "timer.h"

namespace YAVIZ{

  void initialization(conduit_cpp::Node& config){

    Timer init_timer;
    std::string xml_file = config["yac/xml_file"].as_string();
    std::string xsd_file = config["yac/xsd_file"].as_string();
    yac_cinit(xml_file.c_str(),
      xsd_file.c_str());
    std::string comp_name = config["yac/comp_name"].as_string();
    int comp_id;
    yac_cdef_comp(comp_name.c_str(), &comp_id);
    config["yac/comp_id"] = comp_id;
    MPI_Comm localcomm;
    yac_cget_localcomm(&localcomm, comp_id);
    MPIHelper::set_comm(localcomm);
    config["catalyst_initialize/catalyst/mpi_comm"] = MPI_Comm_c2f(localcomm);

    // set global verbosity
    int verbose = config["verbose"].to_int();
    OutStream::setVerbosity(verbose);
    OutStream::activateProfiling(config["profiling"].to_int());

    if(verbose >= 3)
      config["catalyst_initialize"].print();
    conduit_cpp::Node init_node = config["catalyst_initialize"];
    catalyst_status result = catalyst_initialize(c_node(&init_node));
    if(result != catalyst_status_ok)
      throw std::runtime_error("catalyst_initialize failed with status code " + std::to_string(result));
    conduit_cpp::Node about;
    result = catalyst_about(c_node(&about));
    infoStr << "using catalyst implementation " << about["catalyst/implementation"].as_string() << std::endl;

    init_timer.toc();
    timingStr << "Initialization: " << init_timer.seconds() << "s" << std::endl;
  }
}
