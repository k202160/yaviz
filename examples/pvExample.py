import os
from paraview.simple import *

renderView1 = GetRenderView()

producer = TrivialProducer(registrationName='example_grid')

display = Show(producer, renderView1)
if len(producer.PointData) > 0:
    field_name = producer.PointData[0].GetName()
    ColorBy(display, ('POINTS', field_name))
else:
    field_name = producer.CellData[0].GetName()
    ColorBy(display, ('CELLS', field_name))

pNG1 = CreateExtractor('PNG', renderView1, registrationName='PNG1')
pNG1.Trigger = 'TimeStep'

pNG1.Writer.FileName = 'RenderView1_{timestep:06d}{camera}.png'
pNG1.Writer.ImageResolution = [1920, 1080]
pNG1.Writer.Format = 'PNG'

# ------------------------------------------------------------------------------
# Catalyst options
from paraview import catalyst
options = catalyst.Options()
options.GlobalTrigger = 'TimeStep'
options.EnableCatalystLive = 1
options.CatalystLiveTrigger = 'TimeStep'

# ------------------------------------------------------------------------------
if __name__ == '__main__':
    from paraview.simple import SaveExtractsUsingCatalystOptions
    # Code for non in-situ environments; if executing in post-processing
    # i.e. non-Catalyst mode, let's generate extracts using Catalyst options
    SaveExtractsUsingCatalystOptions(options)
