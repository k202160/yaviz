#include "mpihelper.h"

namespace YAVIZ {

  MPIHelper::MPIHelper(int* argc, char*** argv){
    MPI_Init(argc, argv);
  }

  MPIHelper::~MPIHelper(){
    if(_comm != MPI_COMM_NULL)
      MPI_Comm_free(&_comm);
    MPI_Finalize();
  }
    
  void MPIHelper::initialize(int* argc, char*** argv){
    if(_instance)
      std::runtime_error("MPIHelper is already initialized!");
    _instance = std::unique_ptr<MPIHelper>(new MPIHelper(argc, argv));
  }

  void MPIHelper::set_comm(MPI_Comm comm){
    _instance->_comm = comm;
  }

  MPI_Comm MPIHelper::get_comm(){
    return _instance->_comm;
  }

  int MPIHelper::size(){
    int s;
    MPI_Comm_size(_instance->_comm, &s);
    return s;
  }

  int MPIHelper::rank(){
    int r;
    MPI_Comm_rank(_instance->_comm, &r);
    return r;
  }

  std::unique_ptr<MPIHelper> MPIHelper::_instance;

}
