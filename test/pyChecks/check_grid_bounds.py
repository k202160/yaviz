
from paraview import catalyst
from paraview.simple import *
import numpy as np

options = catalyst.Options()

channel = catalyst.get_args()[0]
print(f"Registering producer for channel {channel}")
producer = TrivialProducer(registrationName=channel)

def catalyst_finalize():
    producer.UpdatePipeline()
    bounds = producer.GetDataInformation().GetBounds()
    args = catalyst.get_args()
    x1 = float(args[1][1:]) if len(args) > 1 else -np.pi
    print(f"check if left bound {bounds[0]} is {x1}")
    if not np.isclose(bounds[0], x1, rtol=1e-03, atol=1e-03, equal_nan=False):
        exit(666)
    x2 = float(args[2][1:]) if len(args) > 2 else np.pi
    print(f"check if right bound {bounds[1]} is {x2}")
    if not np.isclose(bounds[1], x2, rtol=1e-03, atol=1e-03, equal_nan=False):
        exit(666)
    y1 = float(args[3][1:]) if len(args) > 3 else -0.5*np.pi
    print(f"check if lower bound {bounds[2]} is {y1}")
    if not np.isclose(bounds[2], y1, rtol=1e-03, atol=1e-03, equal_nan=False):
        exit(666)
    y2 = float(args[4][1:]) if len(args) > 4 else 0.5*np.pi
    print(f"check if upper bound {bounds[3]} is {y2}")
    if not np.isclose(bounds[3], y2, rtol=1e-03, atol=1e-03, equal_nan=False):
        exit(666)
    z1 = float(args[5][1:]) if len(args) > 5 else 0.
    print(f"check if lower z-bound {bounds[4]} is {z1}")
    if not np.isclose(bounds[4], z1, rtol=1e-03, atol=1e-03, equal_nan=False):
        exit(666)
    z2 = float(args[6][1:]) if len(args) > 6 else 0
    print(f"check if upper z-bound {bounds[5]} is {z2}")
    if not np.isclose(bounds[5], z2, rtol=1e-03, atol=1e-03, equal_nan=False):
        exit(666)

    print("Test passed!")
