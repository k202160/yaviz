#ifndef MPIHELPER_H
#define MPIHELPER_H

#include <memory>
#include <mpi.h>

namespace YAVIZ {

  class MPIHelper{
  protected:
    MPIHelper() = delete;
    MPIHelper(const MPIHelper&) = delete;
    MPIHelper& operator=(const MPIHelper&) = delete;

    MPIHelper(int* argc, char*** argv);
  public:
    ~MPIHelper();

    static void initialize(int* argc, char*** argv);

    static void set_comm(MPI_Comm comm);

    static MPI_Comm get_comm();
    
    static int size();

    static int rank();

  protected:
    static std::unique_ptr<MPIHelper> _instance;
    MPI_Comm _comm = MPI_COMM_NULL;
  };
}

#endif
