IF( DEFINED ENV{yac_DIR} )
  SET( yac_DIR "$ENV{yac_DIR}" )
ENDIF()

set(ENV{PKG_CONFIG_PATH} "$ENV{PKG_CONFIG_PATH}:${yac_DIR}/lib/pkgconfig:${yac_DIR}/src/pkgconfig")

# use pkg-config to get the directories and then use these values
# in the find_path() and find_library() calls
find_package(PkgConfig QUIET)
PKG_CHECK_MODULES(PC_yac QUIET yac)

set(yac_COMPILE_OPTIONS ${PC_yac_CFLAGS_OTHER})
set(yac_LINK_LIBRARIES ${PC_yac_LINK_LIBRARIES})
set(yac_VERSION ${PC_yac_VERSION})

find_path(yac_INCLUDE_DIR
  NAMES  yac_interface.h
  HINTS  ${PC_yac_INCLUDE_DIRS} ${yac_DIR}/src ${yac_DIR}/include
)

find_library(yac_LIBRARY
  NAMES
    yac yac_mtime
  HINTS
    ${PC_yac_LIBRARY_DIRS} ${yac_DIR}/src
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(yac
  FOUND_VAR  yac_FOUND
  REQUIRED_VARS  yac_LIBRARY yac_INCLUDE_DIR yac_LINK_LIBRARIES
  VERSION_VAR  yac_VERSION
)

get_filename_component(yac_LIBDIR "${yac_LIBRARY}" DIRECTORY)

if(yac_FOUND AND NOT TARGET yac::yac)
  add_library(yac::yac UNKNOWN IMPORTED)
  set_target_properties(yac::yac PROPERTIES
    IMPORTED_LOCATION "${yac_LIBRARY}"
    INTERFACE_LINK_LIBRARIES "${yac_LINK_LIBRARIES}"
    INTERFACE_COMPILE_OPTIONS "${yac_COMPILE_OPTIONS}"
    INTERFACE_INCLUDE_DIRECTORIES "${yac_INCLUDE_DIR}"
    INTERFACE_LINK_DIRECTORIES "${yac_LIBDIR}"
  )
  foreach(lib ${PC_yac_LDFLAGS_OTHER})
    set_property(TARGET yac::yac APPEND PROPERTY INTERFACE_LINK_LIBRARIES "${lib}")
  endforeach()
endif()

mark_as_advanced(yac_LIBRARY yac_INCLUDE_DIR)

if(yac_FOUND)
  set(yac_LIBRARIES ${yac_LIBRARY})
  set(yac_INCLUDE_DIRS ${yac_INCLUDE_DIR})
endif()
