#include <vector>
#include <string>

#include <catalyst_conduit.hpp>

#include "mpihelper.h"

extern "C"{
#include <yac_interface.h>
}

#include "outstream.h"
#include "timer.h"

namespace YAVIZ {

  void cdi_grid_factory(conduit_cpp::Node& grid_node); // impl. in cdi_grid.cpp
  void uniform_grid_factory(conduit_cpp::Node& grid_node); // impl. in uniform_grid.cpp

  int find_transient_id(int yac_comp_id, const std::string& field_name){
    int t_count = yac_get_transients_count(yac_comp_id);
    std::vector<int> t_ids(t_count);
    yac_get_transient_ids(yac_comp_id, t_ids.data());
    for(int t_id : t_ids){
      if(yac_get_transient_name(t_id) == field_name)
        return t_id;
    }
    throw std::runtime_error("Cannot find transient id of field " + field_name);
  }

  void register_field(conduit_cpp::Node& field_node, int points_id, int comp_id){

    // we're restricting to only one pointsset here for now...
    int field_id;
    yac_cdef_field(field_node.name().c_str(), comp_id,
      &points_id, 1, &field_id);
    field_node["yac_field_id"] = field_id;

    // determine field size (including collection size)
    int t_id = find_transient_id(comp_id, field_node.name());
    int collection_size = yac_get_transient_collection_size(t_id);
    field_node["yac_collection_size"] = collection_size;
    int points_size = yac_get_points_size(points_id);
    field_node["yac_point_size"] = points_size;

    if(!field_node["vector_field"].to_int()){
      field_node["values"] = std::vector<double>(collection_size*points_size);
    }else{
      throw std::runtime_error("TODO");
    }
  }

  void grid_factory(conduit_cpp::Node& grid_node){
    // dispatch type
    if(!grid_node.has_path("type")){
      throw std::runtime_error("cannot parse grid. type node missing.");
    }
    std::string type = grid_node["type"].as_string();
    if(type == "mesh"){
      std::string coord_type = grid_node["data/coordsets/coords/type"].as_string();
      if(coord_type == "uniform")
        return uniform_grid_factory(grid_node);
      if(coord_type == "cdi")
        return cdi_grid_factory(grid_node);
      else
        throw std::runtime_error("unknown coords type (" + coord_type + ")");
    }
    throw std::runtime_error("unknown grid type");
  }

  void prepare(conduit_cpp::Node& config){
    Timer yac_init_timer;

    //////////////////////////////////////
    // Register grids and fields at YAC //
    //////////////////////////////////////
    conduit_cpp::Node execute_node = config["catalyst_execute"];
    int nbr_children = execute_node["catalyst/channels"].number_of_children();
    for(int c = 0; c < nbr_children; ++c){
      conduit_cpp::Node grid_node = execute_node["catalyst/channels"].child(c);
      infoStr << "Prepare grid " << grid_node.name() << "..." << std::endl;

      grid_factory(grid_node);

      int nbr_fields = grid_node["data/fields"].number_of_children();
      for(int f = 0; f < nbr_fields; ++f){
        conduit_cpp::Node field_node = grid_node["data/fields"].child(f);
        if(field_node.has_child("values")){
          infoStr << "Field " << field_node.name() << "has already values and is"
            " considered constant" << std::endl;
          continue;
        }
        infoStr << "Prepare field " << field_node.name() << "..." << std::endl;
        int points_id;
        if(field_node["association"].as_string() == "vertex")
          points_id = grid_node["yac_corner_points_id"].to_int();
        else if(field_node["association"].as_string() == "element")
          points_id = grid_node["yac_cell_points_id"].to_int();
        else
          throw std::runtime_error("Unknown value association");
        register_field(field_node, points_id, config["yac/comp_id"].to_int());
      }
    }

    timingStr << "Preperation: " << yac_init_timer.seconds() << "s" << std::endl;
  }

}
