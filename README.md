# YAVIZ

Yet Another VIZualization interface

## Dependencies:
- CMake
- C++ 17
- MPI
- [catalyst](https://www.paraview.org/in-situ/)
- [libcdi](https://gitlab.dkrz.de/mpim-sw/libcdi)
  For reading grid files.
- [YAC](https://gitlab.dkrz.de/dkrz/yac)
  
## How does YAVIZ work?
YAVIZ forwards data from YAC to the catalyst interface. For that it loads an conduit tree, amends it with data obtained during runtime and calls the catalyst interface functions with the corresponding subtrees.

The whole process is described as follows:
- Initialization
  - Reading the configuration file (yaviz.yaml or passed as a commandline parameter)
  - initialize YAC by calling `yac_cinit` and `yac_cdef_comp`
  - put the local mpi communicator (obtained from `yac_cget_localcomm`) into the configuration tree (`catalyst_initialize/catalyst/mpi_comm`)
  - call `catalyst_initialize`
- Preparation
  - For each `channel` in the `catalyst_execute/channels` node the corresponding grid is registered at yac (see [Grids](#grids))
	- For each field in the channel this field is registered at yac and a `value` node with the correct size is created
- `yac_csearch`
- Execution
  - YAVIZ uses the mtime library for the scheduling the events for calling `yac_get` and `catalyst_execute`
  - If no timestep for YAVIZ is specified in the configuration, `catalyst_execute` is called after each event
- Finalization
  - call `catalyst_finalize`
  - call `yac_cfinalize`

The basic structure of the conduit tree is the following:
- `verbose`: specifies the verbosity of YAVIZ
- `profiling`: prints timing information for the different stages
- `timestep`: timestep for calling `catalyst_execute` in the timeloop. If not set, it is called after every event.
- `yac`
  - `xml_file`: xml file containing the YAC coupling configuration
  - `xsd_file`: the xml-schema file
  - `comp_name`: The component name for registering YAVIZ at YAC
- `catalyst_initialize`: forwarded to `catalyst_initialize`
- `catalyst_execute`: forwarded to `catalyst_execute`
- `catalyst_finalize`: forwarded to `catalyst_finalize`

### Configuration extention
The conduit nodes that are passed to the `catalyst_` functions are extended by YAVIZ with the following values:
- `catalyst_initialize`:
  - `catalyst/mpi_comm`: The MPI communicator returned by `yac_cget_localcomm`
- `catalyst_execute`:
  - `catalyst/channels/{gridname}`: see [Grids](#grids)
  - `fields/{fieldname}/values`: corresponding to the grid, the memory for storing the values is allocated and set as external values to this node.

## Grids
### Uniform Grid
Appears at `Image Data` on the paraview site. Example:
```yaml
example_grid:
  type: "mesh"
    data:
      coordsets:
        coords:
          type: uniform
          dims:
            i: 200
            j: 100
            k: 2
          origin:
            x: -3.14159
            y: -1.5
            z: 0
          spacing:
            dx: 0.0314159
            dy: 0.0314159
            dz: 1
      topologies:
        mesh:
          type: "uniform"
          coordset: "coords"
```

### Unstructured Grid from CDI
Example:
```yaml
example_grid:
  type: mesh
  cdi_filename: icon_grid_0030_R02B03_G.nc
  projection: sphere
  varname: cell_area
  zaxis:
    levels: 42
    dz: -0.1
    oz: 4.2
  data:
    coordsets:
      coords:
        type: cdi
```
The `varname` only used for obtaining the correct grid id from the file.
Optionally the parameter `projection` can be specified as `spherical` (default) or `latlon` to specify the projection. 
More elaborate projection can be created with `latlon` and a proper paraview filter.

## Z-Axis
The zaxis corresponds to the collection size in yac. Note that for example in ICON the collection starts with the upper level.

## Resources
- [Catalyst documentation](https://catalyst-in-situ.readthedocs.io/en/latest/index.html)
- [Paraview Blueprint Documentation](https://kitware.github.io/paraview-docs/latest/cxx/ParaViewCatalystBlueprint.html)
- [YAC documentation](https://dkrz-sw.gitlab-pages.dkrz.de/yac/index.html)

## Catalyst Implementation
The catalyst API can be implemented by different implementation. By default the so-called stub implementation is used, which is a dummy implementation (see [Catalyst documentation](https://catalyst-in-situ.readthedocs.io/en/latest/index.html))
There are two options for making catalyst to use the paraview implementation:
- Setting the environment variables
  - `CATALYST_IMPLEMENTATION_NAME=paraview`
  - `CATALYST_IMPLEMENTATION_PATHS=/path/to/libcatalyst-paraview.so` (only the path, not the filename)
- Setting the values in the conduit tree
```yaml
catalyst_initialize:
  catalyst_load:
    implementation: paraview
	search_paths: /path/to/libcatalyst-paraview.so (only the path, not the filename)
```

## Troubleshooting
- Increase the verbosity of catalyst. In particular helpful if the wrong implementation is used by catalyst.
```
export CATALYST_DEBUG=1
```
- The verbosity of catalyst-paraview can be increased with
```
export PARAVIEW_LOG_CATALYST_VERBOSITY=INFO
```

## TODO
- structured CDI grids (latlon/Gaussian)
- timestepping with respect to yac timestamps (use mtime?)
- add information for catalyst about domain decomposition (multimesh?)
- Add option to tetrahedralize a cdi grid for volume rendering (only vertex associated data is allowed then)
