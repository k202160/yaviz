IF( DEFINED ENV{mtime_DIR} )
  SET( mtime_DIR "$ENV{mtime_DIR}" )
ENDIF()

find_path(mtime_INCLUDE_DIR NAMES mtime_time.h
  DOC "The mtime include directory"
  HINTS ${mtime_DIR}/include
)

find_library(mtime_LIBRARY NAMES mtime
  DOC "The mtime library"
  HINTS ${mtime_DIR}/lib
)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(mtime
                                  REQUIRED_VARS mtime_LIBRARY mtime_INCLUDE_DIR)

if(mtime_FOUND)
  set( mtime_LIBRARIES ${mtime_LIBRARY} )
  set( mtime_INCLUDE_DIRS ${mtime_INCLUDE_DIR} )
  if(NOT TARGET mtime::mtime)
    add_library(mtime::mtime UNKNOWN IMPORTED)
    set_target_properties(mtime::mtime PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${mtime_INCLUDE_DIRS}")
    set_property(TARGET mtime::mtime APPEND PROPERTY IMPORTED_LOCATION "${mtime_LIBRARY}")
  endif()
endif()

mark_as_advanced(mtime_INCLUDE_DIR mtime_LIBRARY)
