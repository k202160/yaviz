
#include <catalyst_conduit.hpp>
#include <catalyst.hpp>

#include "mpihelper.h"
extern "C" {
#include "yac_interface.h"
}

#include "outstream.h"
#include "timer.h"

namespace YAVIZ {

  void finalize(conduit_cpp::Node& config){
    infoStr << "Finalizing..." << std::endl;
    Timer cat_fin_timer;
    conduit_cpp::Node finalize_node = config["catalyst_finalization"];
    catalyst_status result = catalyst_finalize(c_node(&finalize_node));
    if(result != catalyst_status_ok)
      throw std::runtime_error("catalyst_finalize failed with status code " + std::to_string(result));
    cat_fin_timer.toc();
    timingStr << "catalyst finalization: " << cat_fin_timer.seconds() << "s" << std::endl;
    Timer yac_fin_timer;
    yac_cfinalize();
    yac_fin_timer.toc();
    timingStr << "YAC finalization: " << yac_fin_timer.seconds() << "s" << std::endl;
  }
}
