#ifndef OUTSTREAM_H
#define OUTSTREAM_H

#include <iostream>

#include "mpihelper.h"

namespace YAVIZ {

  /* Output stream that filters reagrding the global verbosity settings.
     Convertion is:
     0 = only errors
     1 = warnings
     2 = infos

     OutStream::globalVerbosity verbosity must be set by the initializaing application
   */
  class OutStream{
    template<class T>
    friend OutStream& operator<<(OutStream&, const T&);
    friend OutStream& operator<< (OutStream &s, std::ostream& (*f)(std::ostream &));
    friend OutStream& operator<< (OutStream &s, std::ostream& (*f)(std::ios &));
    friend OutStream& operator<< (OutStream &s, std::ostream& (*f)(std::ios_base &));
  public:
    explicit OutStream(std::ostream& stream, int color = 39 /*=default*/)
      : _outStream(stream)
      , _color(color)
    {}

    static void setVerbosity(int v);

    static void activateProfiling(bool active = true);

  protected:
    bool active = false;
    std::ostream& _outStream = std::cout;
    int _color = 39; /*=black*/
  };

  template<class T>
  OutStream& operator<<(OutStream& s, const T& obj){
    if(s.active){
      s._outStream << "\033["<< s._color << "m" << obj << "\033[39m";
    }
    return s;
  }

  extern OutStream warnStr;
  extern OutStream infoStr;
  extern OutStream timingStr;

}

#endif
