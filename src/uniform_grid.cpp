#include <array>
#include <vector>
#include <algorithm>

#include <catalyst_conduit.hpp>

#include "mpihelper.h"

extern "C" {
#include <yac_interface.h>
}

namespace YAVIZ {

  inline std::vector<double> linspace(double x0, double dx, int nx){
    std::vector<double> space(nx);
    for(size_t i=0; i<space.size(); ++i){
      space[i] = x0+i*dx;
    }
    return space;
  }

  inline void distribute(conduit_cpp::Node& grid_node){
    int nprocs = MPIHelper::size();
    int rank = MPIHelper::rank();
    // distributing the cells (!)
    int i = grid_node["coordsets/coords/dims/i"].to_int();
    int nbr_cells = (i-1)/nprocs;

    // compute local origin
    double ox = grid_node["coordsets/coords/origin/x"].to_double();
    double dx = grid_node["coordsets/coords/spacing/dx"].to_double();
    grid_node["coordsets/coords/origin/x"] = ox+rank*nbr_cells*dx;

    if(rank == nprocs-1)
      nbr_cells = (i-1)-(nprocs-1)*nbr_cells; // remainder

    grid_node["coordsets/coords/dims/i"] = nbr_cells+1;
  }

  void uniform_grid_factory(conduit_cpp::Node& grid_node){
    conduit_cpp::Node data_node = grid_node["data"];
    int global_i = data_node["coordsets/coords/dims/i"].to_int();

    distribute(data_node);

    double ox = data_node["coordsets/coords/origin/x"].to_double();
    double oy = data_node["coordsets/coords/origin/y"].to_double();
    double dx = data_node["coordsets/coords/spacing/dx"].to_double();
    double dy = data_node["coordsets/coords/spacing/dy"].to_double();
    int i = data_node["coordsets/coords/dims/i"].to_int();
    int j = data_node["coordsets/coords/dims/j"].to_int();

    std::vector<double> x_vertices = linspace(ox, dx, i);
    std::vector<double> y_vertices = linspace(oy, dy, j);

    std::array<int, 2> cyclic = {0,0};
    std::array<int, 2> nbr_points = {i,j};
    int yac_grid_id;
    yac_cdef_grid_reg2d(grid_node.name().c_str(), &nbr_points[0], &cyclic[0],
			x_vertices.data(), y_vertices.data(), &yac_grid_id);
    grid_node["yac_grid_id"] = yac_grid_id;


    int corner_points_id;
    yac_cdef_points_reg2d(yac_grid_id, &nbr_points[0], YAC_LOCATION_CORNER,
			  x_vertices.data(), y_vertices.data(), &corner_points_id);
    grid_node["yac_corner_points_id"] = corner_points_id;
    // compute global indices (YAC defines the points row wise (x-coodinates fastest))
    int nprocs = MPIHelper::size();
    int rank = MPIHelper::rank();
    std::vector<int> idxs(i*j);
    int nbr_local_cells = (global_i-1)/nprocs;
    int idx = (rank*nbr_local_cells); // first index
    size_t k=0;
    for(int jj=0; jj<j; ++jj){
      for(int ii=0; ii<i; ++ii){
	idxs[k++] = idx++;
      }
      idx += global_i - i;
    }
    yac_cset_global_index(idxs.data(), YAC_LOCATION_CORNER, yac_grid_id);

    // move vertices to centers
    nbr_points[0]--;
    nbr_points[1]--;
    std::transform(x_vertices.begin(), x_vertices.end(),
		   x_vertices.begin(), [&](double x) {return x+dx/2;});
    std::transform(y_vertices.begin(), y_vertices.end(),
		   y_vertices.begin(), [&](double y) {return y+dy/2;});
    int cell_points_id;
    yac_cdef_points_reg2d(yac_grid_id, &nbr_points[0], YAC_LOCATION_CELL,
			  x_vertices.data(), y_vertices.data(), &cell_points_id);
    grid_node["yac_cell_points_id"] = cell_points_id;

    idxs.resize((i-1)*(j-1));
    idx = (rank*nbr_local_cells); // first index
    k=0;
    for(int jj=0; jj<j-1; ++jj){
      for(int ii=0; ii<i-1; ++ii){
	idxs[k++] = idx++;
      }
      idx += (global_i-1) - (i-1);
    }
    yac_cset_global_index(idxs.data(), YAC_LOCATION_CELL, yac_grid_id);
  }
}
